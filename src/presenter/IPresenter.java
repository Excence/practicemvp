package presenter;

public interface IPresenter {
    void add(int a, int ind);
    void addArray(int[] inn);
    void addStart(int a);
    void addEnd(int a);
    void del(int ind);
    void delStart();
    void delEnd();
    void clear();
    void print();
    void setArr(int[] arr);
    void sortMinToMax();
    void sortMaxToMin();
    void reverse();
    void changeHalf();
}
