package presenter;

import model.DynamicArray;
import model.IDynamicArray;
import view.IView;

public class Presenter implements  IPresenter {
    private IView view;
    private DynamicArray dynamicArray;

    public Presenter (IView view){
        this.view = view;
        this.dynamicArray = new DynamicArray();
    }


    @Override
    public void add(int a, int ind) {
        dynamicArray.add(a,ind);
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void addArray(int[] inn) {
        dynamicArray.addArray(inn);
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void addStart(int a) {
        dynamicArray.addStart(a);
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void addEnd(int a) {
        dynamicArray.addEnd(a);
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void del(int ind) {
        dynamicArray.del(ind);
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void delStart() {
        dynamicArray.delStart();
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void delEnd() {
        dynamicArray.delEnd();
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void clear() {
        dynamicArray.clear();
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void print() {
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void setArr(int[] arr) {
        dynamicArray.setArr(arr);
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void sortMinToMax() {
        dynamicArray.sortMinToMax();
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void sortMaxToMin() {
        dynamicArray.sortMaxToMin();
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void reverse() {
        dynamicArray.reverse();
        view.showMessage(dynamicArray.intoString());
    }

    @Override
    public void changeHalf() {
        dynamicArray.changeHalf();
        view.showMessage(dynamicArray.intoString());
    }
}
