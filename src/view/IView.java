package view;

public interface IView {
    void showMessage(String message);
}
