package view;

import presenter.IPresenter;
import presenter.Presenter;

public class View implements IView {


    public static void main(String[] args) {
        View view = new View();
        IPresenter presenter = new Presenter(view);
        presenter.setArr(new int[]{5,0,-5});
        presenter.addArray(new int[]{9, 7, 4, 8 ,12});
        presenter.add(15, 3);
        presenter.addEnd(6);
        presenter.addStart(33);
        presenter.changeHalf();
        presenter.reverse();
        presenter.sortMaxToMin();
        presenter.sortMinToMax();
        presenter.del(5);
        presenter.delStart();
        presenter.delEnd();
        presenter.print();
        presenter.clear();

    }
    @Override
    public void showMessage(String message) {
        System.out.println(message);
    }
}