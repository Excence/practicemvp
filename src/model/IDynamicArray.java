package model;

public interface IDynamicArray {
    void add(int a, int ind);
    void addArray(int[] inn);
    void addStart(int a);
    void addEnd(int a);
    void del(int ind);
    void delStart();
    void delEnd();
    void clear();
    int[] getArray();
    String intoString();
    void print();
}
