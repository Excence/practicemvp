package model;

public class BasicArray {
    protected  int[] arr;

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public void sortMinToMax() {
        int buf;

        for (int i = arr.length - 1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    buf = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = buf;
                }
            }
        }
    }

    public void sortMaxToMin() {
        int buf;

        for (int i = arr.length - 1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    buf = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = buf;
                }
            }
        }
    }

    public void reverse() {
        int buf;

        for (int i = 0; i < arr.length / 2; i++) {
            buf = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = buf;
        }
    }

    public void changeHalf() {
        int buf;

        for (int i = 0; i < arr.length / 2; i++) {
            buf = arr[i];
            arr[i] = arr[arr.length / 2 + i];
            arr[arr.length / 2 + i] = buf;
        }
    }
}
