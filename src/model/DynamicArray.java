package model;

public class DynamicArray extends BasicArray implements IDynamicArray {
    @Override
    public void add(int a, int ind) {
        if (ind < 0){
            System.out.println("Введен отрицательный индекс");
            return;
        }

        int[] buf = new int[arr.length + 1];

        if (arr.length == 0){ // если массив пустой
            buf[0] = a;
            arr = buf;
            return;
        }

        if (ind > arr.length){ // если указаный индекс больше размера массива
            addEnd(a);
            return;
        }

        for (int i = 0; i < buf.length; i++) { // классический вариант
            if (i < ind) {
                buf[i] = arr[i];
            }
            if (i > ind) {
                buf[i] = arr[i-1];
            }
        }
        buf[ind] = a;
        arr = buf;
    }

    @Override
    public void addArray(int[] inn) {
        if (arr.length == 0) {
            arr = inn;
            return;
        }
        for (int i = 0; i < inn.length; i++) {
            addEnd(inn[i]);
        }
    }

    @Override
    public void addStart(int a) {
        int[] buf = new int[arr.length + 1];
        buf[0] = a;
        for (int i = 0; i < arr.length; i++) {
            buf[i + 1] = arr[i];
        }
        arr = buf;
    }

    @Override
    public void addEnd(int a) {
        int[] buf = new int[arr.length + 1];
        buf[buf.length - 1] = a;
        for (int i = 0; i < arr.length; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    @Override
    public void del(int ind) {
        if (ind < 0){
            System.out.println("Введен отрицательный индекс");
            return;
        }

        if (arr.length == 0){
            System.out.println("Удаление элемента невозможно: массив пустой");
            return;
        }

        if (ind > arr.length){
            System.out.println("Удаление элемента невозможно: в массиве меньше элементов");
            return;
        }

        int[] buf = new int[arr.length - 1];

        for (int i = 0; i < arr.length; i++) {
            if (i < ind){
                buf[i] = arr[i];
            }
            if (i > ind){
                buf[i - 1] = arr[i];
            }
        }
        arr = buf;
    }


    @Override
    public void delStart() {
        int[] buf = new int[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i + 1];
        }
        arr = buf;
    }

    @Override
    public void delEnd() {
        int[] buf = new int[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    @Override
    public void clear() {
        int[] buf = {};
        arr = buf;
    }

    @Override
    public int[] getArray() {
        return arr;
    }

    @Override
    public String intoString() {
        String str = new String();
        if(arr.length != 0){
            str += "[";
            for (int i = 0; i < arr.length-1; i++) {
                str += arr[i] + ", ";
            }
            str += arr[arr.length-1] + "]";
        } else {
            str = "Массив пустой";
        }
        return str;
    }

    @Override
    public void print() {
        if (arr.length != 0) {
            System.out.print("[");
            for (int i = 0; i < arr.length - 1; i++) {
                System.out.print(arr[i] + ", ");
            }
            System.out.println(arr[arr.length - 1] + "]");
        } else {
            System.out.println("Массив пустой");
        }
    }
}
